<?php

/**
 * @module          anynews
 * @author          cwsoft, LEPTON project
 * @copyright       cwsoft, LEPTON project
 * @link            https://cms-lab.com
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

// OBLIGATORY VARIABLES
$module_directory   = 'anynews';
$module_name        = 'Anynews';
$module_function    = 'snippet';
$module_version     = '5.5.2';
$module_status      = 'stable';
$module_platform    = '7.x';
$module_author      = 'cwsoft, LEPTON project (last)';
$module_license     = '<a href="https://gnu.org/licenses/gpl.html">GNU General Public Licencse 3.0</a>';
$module_license_terms = '-';
$module_description = 'Call displayNewsItems(); from the index.php of your template or a code section to display news entries where you want them to be';
$module_guid        = '6886a21d-c3b9-4fec-a46e-e7f6e91a3ec9';
