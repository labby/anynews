<?php

/**
 * @module          anynews
 * @author          cwsoft, LEPTON project
 * @copyright       cwsoft, LEPTON project
 * @link            https://cms-lab.com
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

include_once LEPTON_PATH .'/modules/droplets/droplets.php'; 

// function to display news items on every page via (invoke function from template or code page)
if (!function_exists('displayNewsItems')) 
{
    function displayNewsItems(
        $group_id = 0,                  // IDs of news to show, matching defined $group_id_type (default:=0, all news, 0..N, or array(2,4,5,N) to limit news to IDs matching $group_id_type)
        $section_id = -1,               // ID of news-section to show, if not -1 all groups are disabled
        $max_news_items = 10,           // maximal number of news shown (default:= 10, min:=1, max:= 999)
        $max_news_length = -1,          // maximal length of the short news text shown (default:=-1 => full news length)
        $display_mode = 1,              // 1:=details (default); 2:=list; 3:=coda-slider; 4:flexslider; 4-98 (custom template: display_mode_X.htt); 99:=cheat sheet
        $lang_id = 'AUTO',              // language file to load and lang_id used if $lang_filter = true (default:= auto, examples: AUTO, DE, EN)
        $strip_tags = true,             // true:=remove tags from short and long text (default:=true); false:=dont strip tags
        $allowed_tags = '<p><a><img>',  // tags not striped off (default:='<p><a><img>')
        $custom_placeholder = false,    // false:= none (default), array('MY_VAR_1' => '%TAG%#', ... 'MY_VAR_N' => '#regex_N#' ...)
        $sort_by = 1,                   // 1:=position (default), 2:=posted_when, 3:=published_when, 4:=random order, 5:=number of comments
        $sort_order = 1,                // 1:=descending (default), 2:=ascending
        $not_older_than = 0,            // 0:=disabled (default), 0-999 (only show news `published_when` date <=x days; 12 hours:=0.5)
        $group_id_type = 'group_id',    // type used by group_id to extract news entries (supported: 'group_id', 'page_id', 'section_id', 'post_id')
        $lang_filter = true             // flag to enable language filter (default:= false, show only news from a news page, which language fits $lang_id)
    )
    {
        global $oLEPTON;
        
        $oAN = anynews::getInstance();
        $oTWIG = lib_twig_box::getInstance();
        $oTWIG->registerModule('anynews');

        /**
         *    Is the first arg an [] we're using this one!
         *    Keep in mind, that all other args may be overwriten/ignore by
         *    the settings inside this array!
         *
         *    e.g.
         *        $config = array( 'max_news_items' => 5 );
         *        displayNewsItems( $config, 23, ...
         *
         *    Only 5 news-items (not 23) are shown, as the first array overwrites the following params!
         *
         */
        $all_args = func_get_args();

        if ( isset($all_args[0]) && is_array($all_args[0]) && (count($all_args) == 1 )) {
            // param 1 is pass as an array! We're using this as our config!
            $defaults = [
                'group_id'           => 0,
                'section_id'         => -1,
                'max_news_items'     => 10,
                'max_news_length'    => -1,
                'display_mode'       => 1,
                'lang_id'            => 'AUTO',
                'strip_tags'         => true,
                'allowed_tags'       => '<p><a><img>',
                'custom_placeholder' => false,
                'sort_by'            => 1,
                'sort_order'         => 1,
                'not_older_than'     => 0,
                'group_id_type'      => 'group_id',
                'lang_filter'        => false
            ];
            // overwrite the defaults within the values  of the $config
            foreach ($all_args[0] as $key => $val)
            {
                $defaults[ $key ] = $val;
            }
            
            foreach ($defaults as $key => $val)
            {
                ${$key} = $val;
            }
        }

        /**
         * Include required Anynews files
         */
        require_once 'code/anynews_functions.php';
        require_once 'thirdparty/truncate.php';

        /**
         * Sanitize user specified function parameters
         */
        sanitizeUserInputs($group_id, 'i{0;0;999}');
        sanitizeUserInputs($section_id, 'i{-1;-1;9999}');
        sanitizeUserInputs($max_news_items, 'i{10;1;999}');
        sanitizeUserInputs($max_news_length, 'i{-1;0;250}');
        sanitizeUserInputs($display_mode, 'i{1;1;99}');
        sanitizeUserInputs($strip_tags, 'b');
        sanitizeUserInputs($allowed_tags, 's{TRIM}');
        sanitizeUserInputs($sort_by, 'i{1;1;6}');
        sanitizeUserInputs($sort_order, 'i{1;1;2}');
        sanitizeUserInputs($not_older_than, 'd{0;0;999}');
        sanitizeUserInputs($group_id_type, 'l{group_id;group_id;page_id;section_id;post_id}');
        sanitizeUserInputs($lang_filter, 'b');
        $lang_id = strtoupper($lang_id ?? 'AUTO');

        if ($lang_id == 'AUTO') 
        {
            $lang_id = LANGUAGE;
        }

        // set template file depending on $display_mode
        $used_template = 'display_mode_'.$display_mode.'.lte';

        // start of  SQL database query for Anynews, file references
        $news_table = TABLE_PREFIX.'mod_news_posts';
        $comments_table = TABLE_PREFIX.'mod_news_comments';

        // work out current server time for published_when and published_until checks)
        $server_time = time();

        $sql = "SELECT t1.*, COUNT(`comment_id`) as `comments`
            FROM `".$news_table."` as t1
            LEFT JOIN `".$comments_table."` as t2
            ON t1.`post_id` = t2.`post_id`
            WHERE t1.`active` = '1' 
            AND (t1.`published_when` = '0' OR t1.`published_when` <= '".$server_time."')
            AND (t1.`published_until` = '0' OR t1.`published_until` >= '".$server_time."')";

        // work out for the not older than option This options allows to restrict the matches to news not older than X days
        if ($not_older_than > 0) 
        {
            $sql_not_older_than = ' AND (t1.`published_when` >= \'' . ($server_time - ($not_older_than * 24 * 60 * 60)) . '\')';
            $sql .= $sql_not_older_than;
        }

        if ($section_id == -1 || $section_id == 0 ) 
        {
            /**
             * Work out SQL query for group_id, limiting news to display depending by defined $news_filter
             *  option 1: $group_id:=0 => '1'
             *  option 2: $group_id:=X => `group_id_type` = 'X'
             *  option 3: $group_id:=array(2,3) => `group_id_type` IN (2,3)
             */
            // show all news items if 0 is contained in group_id array
            if (is_array($group_id) && in_array(0, $group_id)) $group_id = 0;

            // check for multiple groups or single group values
            if (is_array($group_id)) 
            {
                // SQL query for multiple groups
                if ($group_id) $sql .= " AND t1.`".$group_id_type."` IN (" . implode(',', $group_id) . ")";
            } 
            else 
            {
                // SQL query for single or empty groups
                if ($group_id) $sql .= " AND t1.`".$group_id_type."` = '$group_id'";
            }
        } 
        else 
        {
            $sql .= " AND t1.`section_id` = ".$section_id;    
        }

        /**
         * Work out SQL query to hide news added via news pages NOT matching $lang_id
         * Returns all news entries if no news page was found matching given $lang_id  
         **/
        if ($lang_filter) 
        {        
            // get all page_ids which page language match defined $lang_id  
            $page_ids = getPageIdsByLanguage($lang_id);
            foreach($page_ids as $ref)
            {
                $temp[] = $ref['page_id'];            
            }        
            
            $sql_lang_filter = '';
            if (!empty($page_ids)) 
            {
                $sql_lang_filter = 't1.`page_id` in (' . implode(',', $temp) . ')';
            }
            $sql .= ' AND '.$sql_lang_filter;
        }


        // Work out News in draft or history, intended suppression of draft and history
        $sIgnoreHistorysPassage = " AND t1.`history_post_id` = -1 "; 

        if (isset($draft))
        {
            if ($draft== "history" ) $sIgnoreHistorysPassage = " AND t1.`history_post_id` = 2 ";
            if ($draft== "draft" ) $sIgnoreHistorysPassage = " AND t1.`history_post_id` = 1 ";
            if ($draft== "all" ) $sIgnoreHistorysPassage = "";
        } 
        $sql .= $sIgnoreHistorysPassage;

        // Work out SQL sort by and sort order query string
        $sql .= " GROUP BY t1.`post_id`";

        // creates SQL query part for sort by option
        $order_by_options = array('t1.`position`', 't1.`posted_when`', 't1.`published_when`', 'RAND()', '`comments`', '`title`'); 
        $sql_order_by = $order_by_options[$sort_by - 1];

        // creates SQL query part for sort order option 
        $sql_sort_order = ($sort_order == 1) ? ' DESC' : ' ASC';

        $sql .= " ORDER BY ".$sql_order_by.$sql_sort_order;

        // creates SQL query part for sort max news items 
        $sql .= " LIMIT 0, ".$max_news_items;
        

        $database = LEPTON_database::getInstance();
        // Process database query and output the template files
        $results = [];
        $database->execute_query(
            $sql,
            true,
            $results,
            true
        );

        /* debug function */ 
        if (isset($debug) && $debug == "sql" )
        {
            echo(LEPTON_tools::display_dev($results,'pre','ui message'));
        }

        if (!empty($results)) 
        {
            $oDT = LEPTON_date::getInstance();
            // get page_language for date-format
            $oDT->setLanguage($oDT->detectPageLanguage());            
            $oDT->setFormat ($oAN->language['DATE_FORMAT']);
            
            // Add also mineTypes from the settings (-table)
            $aImageTypes = LEPTON_core::imageTypesAllowed();
			
            // loop through all news articles found
            $news_counter = 1;
            foreach($results as &$row) 
            {
                // build absolute links from [page_link] tags found in news short or long text database field
                $oLEPTON->preprocess($row['content_short']);
                $oLEPTON->preprocess($row['content_long']);

                $row['content_short'] = str_replace( [ "&amp;", "&lt;","&gt;"], ["&", "<", ">" ], $row['content_short'] ); 
                $row['content_long']  = str_replace( [ "&amp;", "&lt;","&gt;"], ["&", "<", ">" ], $row['content_long'] ); 

                $row['published_when_timestamp'] = $row['published_when'];

                 // fetch custom placeholders from short/long text fields and replace template placeholders with values
                $custom_vars_short_text = getCustomOutputVariables($row['content_short'], $custom_placeholder, 'SHORT');
                $custom_vars_long_text = getCustomOutputVariables($row['content_long'], $custom_placeholder, 'LONG');
                $custom_vars = array_merge($custom_vars_short_text, $custom_vars_long_text);

                // replace custom placeholders in template with values
                foreach ($custom_vars as $key => $value) 
                {
                     $row[strtolower($key)] = $value;    //  gsm added custom values if exists V20190913
                }

                // remove tags from short and long text if defined
                $row['content_short'] = ($strip_tags) ? strip_tags($row['content_short'], $allowed_tags) : $row['content_short'];
                $row['content_long'] = ($strip_tags) ? strip_tags($row['content_long'], $allowed_tags) : $row['content_long'];

                // Droplets
                evalDroplets($row['content_short']);

                // shorten news text to defined news length (-1 for full text length)
                if ($max_news_length != -1 && strlen($row['content_short']) > $max_news_length) 
                {
                    // consider start position if short content starts with <p> or <div>
                    $start_pos = (preg_match('#^(<(p|div)>)#', $row['content_short'], $match)) ? strlen($match[0]) : 0;
                    $row['content_short'] = truncate(substr($row['content_short'], $start_pos), $max_news_length, '...', false, true);
                }

                $row['group_title'] = $oAN->allGroups[ $row['group_id'] ]['title'] ?? ''; 

                // [2.1] work out group image if exists
                $group_id = $row['group_id'];
                $image = '';
                foreach ($aImageTypes as $tempType)
                {
                    if (file_exists(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/groups/image'.$group_id.'.'.$tempType)) 
                    {
                        $image = '<img src="'.LEPTON_URL.MEDIA_DIRECTORY.'/newspics/groups/image'.$group_id.'.'.$tempType.'" alt="'.$row['group_title'].'" />';
                        $row['group_image'] = $image;
                        break;
                    }
                }

                // [2.2] work out post image if exists 
                $post_id = $row['post_id'];
                $image = '';
                foreach ($aImageTypes as $tempType)
                {
                    if (file_exists(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/image'.$post_id.'.'.$tempType)) 
                    {
                        $image = '<img src="'.LEPTON_URL.MEDIA_DIRECTORY.'/newspics/image'.$post_id.'.'.$tempType.'" alt="'.$row['title'].'" />';
                        $row['post_image'] = $image;
                        break;
                    }
                    elseif (isset($row["history_post_id"]) && file_exists(LEPTON_PATH.MEDIA_DIRECTORY.'/newspics/image'.$row["history_post_id"].'.'.$tempType)) 
                    { 
                        $image = '<img src="'.LEPTON_URL.MEDIA_DIRECTORY.'/newspics/image'.$row ["history_post_id"].'.'.$tempType.'" alt="'.$row['title'].'" />';
                        $row['post_image'] = $image;
                        break;
                    }
                }

                // get user name and display_name by the id
                $aUserInfo = $oAN->getUserInfo( $row['posted_by'] );
                $row['username']        = $aUserInfo['username'];
                $row['display_name']    = $aUserInfo['display_name'];

                $row['link'] = LEPTON_URL . PAGES_DIRECTORY . $row['link'] . PAGE_EXTENSION;

                // @notice: see: https://www.w3schools.com/sql/func_mysql_date_format.asp
                $row['post_published_when_full'] = $oDT->formatWithMySQL('%e. %M %Y', $row['published_when_timestamp'], LANGUAGE);

                //  Aldus 2022-10-06: new formats with L*5.5.0
                $row['posted_when']     = $oDT->formatWithMySQL('%d.%m.%Y', $row['posted_when']);
                $row['published_when']  = $oDT->formatWithMySQL('%d.%m.%Y', $row['published_when']);
                $row['published_until'] = $oDT->formatWithMySQL('%d.%m.%Y', $row['published_until']);     

                $row['news_items'] = $news_counter - 1;            
                $row['news_counter'] = $news_counter;
                $news_counter++;
            }

            $data = [
                'lang_id'   => $lang_id,
                'results'   => $results,
                'oAN'       => $oAN
            ];

            /* debug function  */ 
            if (isset($debug) && $debug == "yes") 
            {
                echo(LEPTON_tools::display_dev($data,'pre','ui message olive'));
            }

            $content = $oTWIG->render( 
                "@anynews/".$used_template."",    //    template-filename
                $data                    //    template-data
            );
            $content = htmlspecialchars_decode($content);

            // http://htmlpurifier.org/docs
            $oPURIFIER = lib_lepton::getToolInstance("htmlpurifier");

            $content = $oPURIFIER->purify($content);

            echo $content;
        }
    }
}
