# Anynews Code Snippet for CMS LEPTON 
The code snippet `Anynews` is designed to fetch news entries from the [LEPTON CMS](https://lepton-cms.org) `news` module. Just call the Anynews function ***displayNewsItems();*** where you want the news output to appear in your frontend. Optional function parameters, HTML templates, content placeholders and CSS definitions allows you to style the news output the way you want. Anynews ships with four templates - including two jQuery sliding effects - ready for use out of the box.

Power users define their own placeholders containing information extracted from the short and/or long `news` module description. Mastering Anynews is possible - but requires you to study the information provided in the section ***Customizing Anynews***.

## Download
The released stable `Anynews` installation packages for the LEPTON CMS can be found on the  [LEPTON homepage](https://lepton-cms.com). It is recommended to install/update to the latest available version listed. Older versions are provided for compatibility reasons with older LEPTON versions and may contain bugs or security issues. 

## License
Anynews is licensed under the [GNU General Public License (GPL) v3.0](https://gnu.org/licenses/gpl-3.0.html).

## Requirements
The minimum requirements to get Anynews running on your LEPTON installation are as follows:

- LEPTON ***7.x*** or higher
- default LEPTON `news` module
- Optional: small modification of your template file to enable jQuery support

## Installation
1. download the [current release](https://lepton-cms.com) LEPTON installation package
2. log into your LEPTON backend and go to the `Add-ons/Modules` section
3. install the downloaded zip archive via the LEPTON installer

### Enable jQuery support (optional)
If you want to use JavaScript effects or jQuery plugins with Anynews, you need to include jquery into your frontend template.
Use the jquery from lib_jquery inside the package.	
	
## Usage
As Anynews is designed to fetch news items from the LEPTON `news` module, you need to add some news entries with the `news` module **before** you can use Anynews. If no news are available, Anynews just outputs the message "No news available yet". Follow the steps below to add some news entries with the LEPTON `news` module.

1. log into your LEPTON backend and go to the `Pages` section
2. create a new page or section of type `News` (set visibility to None)
3. add some news entries (2-3) from the `news` page in the LEPTON backend

### Use Anynews from a page or section
Create a new page or section of type `Code` in the LEPTON backend and enter the following code to it.
``` php 
	if (function_exists('displayNewsItems')) {
		displayNewsItems();
	}
```
The Anynews output is only visible at the pages/sections of your frontend, which contain the code above.

### Use Anynews from your template
To display news items at a fixed position on every page of your frontend, open the ***index.php*** file of your default frontend template and add the code below to the position in your template where you want the news output to appear.

``` php
    if (function_exists('displayNewsItems'))
    {
        displayNewsItems();
    }
```
Visit the frontend of your webiste and check the Anynews output.

## Customizing Anynews  
The Anynews output can be customized to your needs by three methods:  

1. parameters of the Anynews _displayNewsItems()_ function
2. customized Anynews template files _templates/display\_mode\_X.lte_  
3. customized CSS definitions in file _/css/anynews.css_  
	
### Anynews Parametes
Calling Anynews in it´s easiest form _displayNewsItems();_ uses the default parameters shown below.

``` php
	displayNewsItems(
		$group_id = 0,
		$section_id = -1,
		$max_news_items = 10,
		$max_news_length = -1,
		$display_mode = 1,
		$lang_id = 'AUTO',
		$strip_tags = true,
		$allowed_tags = '<p><a><img>',
		$custom_placeholder = false,
		$sort_by = 1,
		$sort_order = 1,
		$not_older_than = 0,
		$group_id_type = 'group_id',
		$lang_filter = false
	);
```

**Function parameters explained:**

- *\$group\_id*: only show news which IDs match given *\$group_id_type* (default 'group_id')  

| Value      | Description                                               |
|------------|-----------------------------------------------------------|
| 0          | all news                                                  |
| 1 .. n     | to limit news to single Id                                |
| [2, 4, 5]  | or array multiple Ids, matching *\$group\_id\_type*       |

	
- *\$section\_id*: only show news from this section (default is -1, means no section)  

| Value      | Description                                               |
|------------|-----------------------------------------------------------|
| -1         | no section _(default)_                                    |
| 1 .. n     | only from this section _(group\_id is set to 0)_          |


- *\$max\_news\_items*: max. number of news entries to show  

| Value      | Description                                               |
|------------|-----------------------------------------------------------|
| 1 .. 999   | number of items to show.                                  |

	
- *\$max\_news\_length*: max. news length to be shown  

| Value      | Description                                               |
|------------|-----------------------------------------------------------|
| -1         | full length                                               |
| 1 ... n    | number of max chars to show                               |


- *\$display\_mode*: ID of the Anynews template to use (/templates/display\_mode\_X.lte)  

| Value      | Description                                                                |
|------------|----------------------------------------------------------------------------|
| 1          | details                                                                    |
| 2          | list                                                                       |
| 3          | better-coda-slider                                                         |
| 4          | flexslider                                                                 |
| 5 .. 98    | custom template _(display\_mode\_X.lte)_                                   |
| 99         | cheat sheet with ALL Anynews placeholders available in the template files. |

	
- *\$lang\_id*: mode to detect language file to use  

| Value      | Description                                               |
|------------|-----------------------------------------------------------|
| _AUTO_     | _system_ specific, e.g. use the current page-language     |
| EN         | any valid LEPTON language file ID: 'DE', 'EN', ...        |


- *\$strip\_tags*: flag to strip tags from news short/long text ***not*** contained in *\$allowed_tags*  

| Value      | Description             |
|------------|-------------------------|
| _true_     | strip tags              |
| _false_    | don't strip tags        |


- *\$allowed\_tags*: tags to keep if *\$strip_tags = true*  
	[default: '&lt;p>&lt;a>&lt;img>']<p></p>

- *\$custom\_placeholder*: create own placeholders for usage in template files  
    _Example:_  

``` php
$custom_placeholder = [
    'MY_IMG'  => '%img%',
    'MY_TAG'  => '%author%',
    'MY_REGEX' => '#(test)#i'
];

```
    
    Stores all image URLs, all text inside &lt;author>;&lt;/author>; tags and all matches  
    of "test" in placeholders:  {PREFIX_MY_IMG_#}, {PREFIX_MY_TAG_#}, {PREFIX_MY_REGEX_#},  
    where ***PREFIX*** is either "SHORT" or "LONG", depending if the match was found in  
    the short/long news text and ***#*** is a number between 1 and the number of matches found<p></p>
	
- *\$sort\_by*: defines the sort criteria for the news items returned  
	[1: position, 2: posted\_when, 3: published_when, 4: random order, 5: number of comments, 6: news title]<p></p>
	
- *\$sort\_order*: defines the sort order of the returned news items  
	[1:descending, 2:=ascending]<p></p>
	
- *\$not\_older\_than*: skips all news items which are older than X days  
	[0: don't skip news items, 0..999: skip news items older than x days (hint: 0.5 --> 12 hours)]<p></p>

- *\$group\_id_type*: sets type used by group\_id to extract news entries from  
	[supported: 'group\_id', 'page\_id', 'section\_id', 'post\_id')]<p></p>

- *\$lang\_filter*: flag to enable language filter   
	[default := false, true := only show news added from news pages, which page language match $lang_id]<p></p>

	
### Anynews Templates
- The HTML skeleton of the Anynews output is defined by template files **templates/display_mode_X.lte**. The template to be used is defined by the Anynews function parameter **$display_mode**, which defaults to 1 if no valid input is defined. Create a blank template file and name as follows: **templates/display_mode_5.lte**

- Use the Contents of template **display_mode_1.lte** or **display_mode_2.lte** as the start of your  **templates/display_mode_5.lte**
 
- Review the template file ***display_mode_99.lte*** (cheat sheet) to get a list of all available Anynews placeholders to populate your template.

- If you want to create a custom template with jQuery effects, look at the template files ***display_mode_3.lte*** and ***display_mode_4.lte***, which implement 3rd party jQuery sliding effects.

#### cheat sheet ***display_mode_99.lte*** 
You have access to the most of the functions of the cheat sheet using the following php code in a code section. 

``` php

$menutegels= [
    'group_id'           => [1, 2, 3],
    'max_news_items'     => 8,
    'max_news_length'    => -1,
    'display_mode'       => 99,
    'strip_tags'         => true,
    'allowed_tags'       => '<h2><h3><h6><p><a><em><br><ul><li><div><strong>',
    'custom_placeholder' => ['MY_TAG' => '%pre%', 'MY_IMG' => '%img%', 'MY_VAR' => '#(debug)#i' ],
    'sort_by'            => 'title',
    'sort_order'         => 2
];

if (function_exists('displayNewsItems'))
{
    displayNewsItems( $menutegels );
}
```

and the following in the long text of your news item
``` html
<pre>/page/contact.php</pre>
<p><img src="/media/newspics/image2.jpg" alt="" /></p>
<p>debug</p>

```

## Questions
If you have questions or issues with Anynews, please visit the [English](https://forum.lepton-cms.org/) LEPTON forum support threads and ask for feedback.

***Always provide the following information with your support request:***

 - detailed error description (what happens, what have you already tried ...)
 - the Anynews version (go to LEPTON section Add-ons / Info / Anynews)
 - your PHP version (use phpinfo(); or ask your provider if in doubt)
 - LEPTON version
 - name of the LEPTON frontend template 
 - information about your operating system (e.g. Windows, Mac, Linux) incl. version
 - information of your browser and browser version used
 - information about changes you made to LEPTON (if any)

