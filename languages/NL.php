<?php

/**
 * @module          anynews
 * @author          cwsoft, LEPTON project
 * @copyright       cwsoft, LEPTON project
 * @link            https://cms-lab.com
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see license
 *
 */

// Dutch module description
$module_description	= 'Snippet om nieuwsberichten te tonen op elke pagina die u maar wilt. Functie kan opgeroepen worden vanuit de template of een code-sectie. Meer informatie is te vinden op <a href="https://gitlab.com/labby/anynews" target="_blank">GitLab</a>.';


$MOD_ANYNEWS = array(
	// text outputs for the frontend
	'TXT_HEADER' => 'Laatste nieuws', 
	'TXT_READMORE' => 'Lees meer', 
	'TXT_NO_NEWS' => 'Geen nieuws beschikbaar.',
	'TXT_NEWS' => 'Nieuws', 
	'TXT_NUMBER_OF_COMMENTS' => 'Aantal reakties', 
	
	// date/time format: please use https://www.php.net/manual/de/function.strftime for formats
	'DATE_FORMAT' => '%A %d. %B %Y'
);