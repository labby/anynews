<?php

/**
 * @module          anynews
 * @author          cwsoft, LEPTON project
 * @copyright       cwsoft, LEPTON project
 * @link            https://cms-lab.com
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see license
 *
 */

// French module description
$module_description	= 'Code pour afficher des news sur différentes pages . Cette fonction peut être apellée depuis le template ou dans une section de code. Plus de détails sur ce module dans le fichier suivant <a href="https://gitlab.com/labby/anynews" target="_blank">GitLab</a>.';


$MOD_ANYNEWS = array(
	// text outputs for the frontend
	'TXT_HEADER' => 'Dernière news', 
	'TXT_READMORE' => 'Lire la suite', 
	'TXT_NO_NEWS' => 'Pas encore de news.',
	'TXT_NEWS' => 'News', 
	'TXT_NUMBER_OF_COMMENTS' => 'Number of comments', 
	
	// date/time format: please use https://www.php.net/manual/de/function.strftime for formats
	'DATE_FORMAT' => '%A %d. %B %Y'
);