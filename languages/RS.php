<?php

/**
 * @module          anynews
 * @author          cwsoft, LEPTON project
 * @copyright       cwsoft, LEPTON project
 * @link            https://cms-lab.com
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see license
 *
 */

// Serbian module description
$module_description	= 'LEPTON addon za prikazivanje novosti na bilo kojoj stranici va�eg web site-a. Funkcija modula mo�e biti implementirana u sam template ili putem code sekcije. Detalji i pomo&#263; oko kori�&#263;enja ovog modula mogu se na&#263;i u samom modulu na linku <a href="https://gitlab.com/labby/anynews" target="_blank">GitLab</a>.';


$MOD_ANYNEWS = array(
	// text outputs for the frontend
	'TXT_HEADER' => 'Najnovije vesti',
	'TXT_READMORE' => 'detaljnije',
	'TXT_NO_NEWS' => 'Vesti ne postoje u bazi.',
	'TXT_NEWS' => 'Novosti',
	'TXT_NUMBER_OF_COMMENTS' => 'Number of comments', 
	
	// date/time format: please use https://www.php.net/manual/de/function.strftime for formats
	'DATE_FORMAT' => '%A %d. %B %Y'
);