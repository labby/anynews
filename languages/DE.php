<?php

/**
 * @module          anynews
 * @author          cwsoft, LEPTON project
 * @copyright       cwsoft, LEPTON project
 * @link            https://cms-lab.com
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see license
 *
 */

// German module description
$module_description	= 'Die Zusatzfunktion Anynews erlaubt das Anzeigen von Beiträgen des News-Moduls auf beliebigen Seiten. Die Funktion kann im Template oder von einer Seite/Abschnitt des Typs Code aufgerufen werden. Weitere Informationen auf <a href="https://gitlab.com/labby/anynews" target="_blank">GitLab</a>.';

$MOD_ANYNEWS = array(
	// text outputs for the frontend
	'TXT_HEADER' => 'Aktuelle Nachrichten', 
	'TXT_READMORE' => 'weiter lesen', 
	'TXT_NO_NEWS' => 'Keine Nachrichten vorhanden.',
	'TXT_NEWS' => 'Nachricht', 
	'TXT_NUMBER_OF_COMMENTS' => 'Anzahl Kommentare', 
	
	// date/time format: please use https://www.php.net/manual/de/function.strftime for formats
	'DATE_FORMAT' => '%A, %d. %B %Y'
);