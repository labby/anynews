<?php

/**
 * @module          anynews
 * @author          cwsoft, LEPTON project
 * @copyright       cwsoft, LEPTON project
 * @link            https://cms-lab.com
 * @license         https://gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see license
 *
 */

// English module description
$module_description		= 'Invoke the function displayNewsItems(); from the index.php of your template or a code section to display news entries where you want them to be. For details see <a href="https://gitlab.com/labby/anynews" target="_blank">GitLab</a>.';


$MOD_ANYNEWS = array(
	// text outputs for the frontend
	'TXT_HEADER' => 'Latest News', 
	'TXT_READMORE' => 'read more', 
	'TXT_NO_NEWS' => 'No news available yet.',
	'TXT_NEWS' => 'News', 
	'TXT_NUMBER_OF_COMMENTS' => 'Number of comments', 
	
	// date/time format: please use https://www.php.net/manual/de/function.strftime for formats
	'DATE_FORMAT' => '%A %d. %B %Y'

);